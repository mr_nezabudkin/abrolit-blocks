$('.carousel').carousel({
    interval: 2000
})

$(document).ready(function() {

    if ($('#slideshow0').length > 0) {

        $('#slideshow0').swiper({
            mode: 'horizontal',
            slidesPerView: 1,
            pagination: '.slideshow0',
            paginationClickable: true,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            spaceBetween: 30,
            autoplay: 2500,
            autoplayDisableOnInteraction: true,
            loop: true
        });

    }

    if ($('#map').length > 0) {

        ymaps.ready(function() {
            var myMap = new ymaps.Map("map", {
                    center: [55.677113, 37.885134],
                    zoom: 16
                }, {
                    searchControlProvider: 'yandex#search'
                }),
                myPlacemark = new ymaps.Placemark([55.677113, 37.882234], {
                    // Р§С‚РѕР±С‹ Р±Р°Р»СѓРЅ Рё С…РёРЅС‚ РѕС‚РєСЂС‹РІР°Р»РёСЃСЊ РЅР° РјРµС‚РєРµ, РЅРµРѕР±С…РѕРґРёРјРѕ Р·Р°РґР°С‚СЊ РµР№ РѕРїСЂРµРґРµР»РµРЅРЅС‹Рµ СЃРІРѕР№СЃС‚РІР°.
                    iconCaption: 'РљРѕРјСЃРѕРјРѕР»СЊСЃРєР°СЏ СѓР»РёС†Р°, 15Рђ\n' +
                    'Р›СЋР±РµСЂС†С‹, РњРѕСЃРєРѕРІСЃРєР°СЏ РѕР±Р»Р°СЃС‚СЊ, Р РѕСЃСЃРёСЏ, 140005'
                    // balloonContentFooter: "РџРѕРґРІР°Р»",
                }, {
                    preset : 'islands#redDotIconWithCaption'
                });

            myMap.geoObjects.add(myPlacemark);
            myMap.behaviors.disable('scrollZoom');
            myMap.controls
                .remove('trafficControl')
                .remove('searchControl')
                .remove('typeSelector')
                .remove('geolocationControl')
                .remove('fullscreenControl')
                .remove('rulerControl');
        });
    }

});