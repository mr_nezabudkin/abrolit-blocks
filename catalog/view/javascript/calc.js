function add_wall() {

    var wall = $('.wall_tr_add');

    var selLength   = Number($(wall).find('.wall_length').val());
    var selHeight   = Number($(wall).find('.wall_height').val());
    var selCount    = Number($(wall).find('.wall_count').val());
    var selCount    = (selCount > 0) ? selCount : 1;
    var selNumb     = Number($(wall).find('#num_wall').val());

    if(selLength > 0 && selHeight > 0){

        $('#wall_table').show();

        var calcS = (selLength+selHeight)*selCount;

        var selNumbPlus = selNumb+1;

        var new_tr_html = '' +
            '<tr class="wall_tr wall_tr_' + selNumbPlus + '">\n' +
            '   <td><button type="button" formaction="" data-toggle="tooltip" title="" class="btn btn-danger"\n' +
            '       onclick="confirm(\'Данное действие необратимо. Вы уверены?\') ? delete_wall(' + selNumbPlus + ') : false;" data-original-title="Удалить"><i class="fa fa-trash-o"></i></button></td>\n' +
            '   <td><input disabled class="btn calc-intp wall_length" value="'+selLength+'" type="text"></td>\n' +
            '   <td><input disabled class="btn calc-intp wall_height" value="'+selHeight+'" type="text"></td>\n' +
            '   <td><input disabled class="btn calc-intp wall_number" value="'+selCount+'" type="text"></td>\n' +
            '   <td class="squareTD">= <span class="btn calc-intp square" id="wallSquare_' + selNumbPlus + '">'+calcS+'</span></td>\n' +
            '</tr>';

        $('#num_wall').val(selNumbPlus);

        var sSquare = $('#wall_table .square');

        if($(sSquare).length > 0) {

            var aSquare = Array.prototype.slice.call(sSquare).reduce( addMe, 0);

            $('#wall_table .allSquare').text( aSquare+calcS )
        }

    $("#wall_table table").append(new_tr_html);

    } else {
        confirm('Ошибка! Вы вводите пустые значения!');
    }

}


function add_aperture() {

    var wall = $('.wall_tr_add');

    var selLength   = Number($(wall).find('.wall_length').val());
    var selHeight   = Number($(wall).find('.wall_height').val());
    var selCount    = Number($(wall).find('.wall_count').val());
    var selCount    = (selCount > 0) ? selCount : 1;
    var selNumb     = Number($(wall).find('#num_wall').val());

    if(selLength > 0 && selHeight > 0){

        $('#aperture_table').show();

        var calcS = (selLength+selHeight)*selCount;

        var selNumbPlus = selNumb+1;

        var new_tr_html = '' +
            '<tr class="aperture_tr wall_tr_' + selNumbPlus + '">\n' +
            '   <td><button type="button" formaction="" data-toggle="tooltip" title="" class="btn btn-danger"\n' +
            '       onclick="confirm(\'Данное действие необратимо. Вы уверены?\') ? delete_wall(' + selNumbPlus + ') : false;" data-original-title="Удалить"><i class="fa fa-trash-o"></i></button></td>\n' +
            '   <td><input disabled class="btn calc-intp wall_length" value="'+selLength+'" type="text"></td>\n' +
            '   <td><input disabled class="btn calc-intp wall_height" value="'+selHeight+'" type="text"></td>\n' +
            '   <td><input disabled class="btn calc-intp wall_number" value="'+selCount+'" type="text"></td>\n' +
            '   <td class="squareTD">= <span class="btn calc-intp square" id="wallSquare_' + selNumbPlus + '">'+calcS+'</span></td>\n' +
            '</tr>';

        $('#num_wall').val(selNumbPlus);

        var sSquare = $('#aperture_table .square');

        if($(sSquare).length > 0) {

            var aSquare = Array.prototype.slice.call(sSquare).reduce( addMe, 0);

            $('#aperturetable .allSquare').text( aSquare+calcS )
        }

        $("#aperture_table table").append(new_tr_html);

    } else {
        confirm('Ошибка! Вы вводите пустые значения!');
    }

}



function addMe(p,c,i,a){
    // p - предыдущее значение общей суммы, c - очередной элемент массива
    return p + parseInt(c.textContent);
}

function add_aperture() {
    var skolko_tr = $(".aperture_tr").length;
    var new_tr_num = skolko_tr + 1;
    var new_tr_html = '<tr class="aperture_tr aperture_tr_' + new_tr_num + '"><td><input class="aperture_width" type="text"></td><td><input class="aperture_height" type="text"></td><td style="position: relative;"><input class="aperture_quantity" type="text"><img class="delete_aperture" id="delete_aperture_' + new_tr_num + '" src="/images/delete.png" title="Удалить проем"></td></tr>';
    $("#aperture_table").append(new_tr_html);
    $(".delete_aperture").click(function() {
        delete_aperture($(this).attr("id"));
    });
}

function delete_wall(id) {
    $(".wall_tr_" + id).remove();
}

function delete_aperture(id) {
    var target_tr_number = id.slice(-1);
    $(".aperture_tr_" + target_tr_number).remove();

}

function add_mansard() {
    var skolko_tr = $(".mansard_tr").length;
    var new_tr_num = skolko_tr + 1;
    var new_tr_html = '<tr class="mansard_tr mansard_tr_' + new_tr_num + '"> <td><input class="mansard_width" type="text"></td><td><input class="mansard_height" type="text"></td><td><input class="mansard_height2" type="text"></td><td style="position: relative;"><input class="mansard_number" id="one" type="text"><img class="delete_mansard" id="delete_mansard_' + new_tr_num + '" src="/images/delete.png" title="Удалить стену"></td></tr>';
    $("#mansard_table").append(new_tr_html);
    $(".delete_mansard").click(function() {
        delete_mansard($(this).attr("id"));
    });
}

function delete_mansard(id) {
    var target_tr_number = id.slice(-1);
    $(".mansard_tr_" + target_tr_number).remove();

}

function calculate() {
    $("#calc_result_numbers").hide();
    $(".progress_containter").show();

    $('.progress').circleProgress({
        value: 1,
        size: 120,
        animation: {
            duration: 250
        },
    }).on('circle-animation-start', function(event, progress) {}).on('circle-animation-progress', function(event, progress) {
        $(this).find('strong').html(parseInt(100 * progress) + '<i>%</i>');
    }).on('circle-animation-end', function(event) {
    });

    var wall_length = '';
    var wall_height = '';
    var wall_number = '';
    var wall_part_result = 0;
    var wall_complete = 1;

    $('.wall_tr').each(function(i, elem) {

        wall_length = $(this).find(".wall_length").val().replace(/[,]+/g, '.');
        wall_height = $(this).find(".wall_height").val().replace(/[,]+/g, '.');
        wall_number = $(this).find(".wall_number").val().replace(/[,]+/g, '.');

        if (Number(wall_number) == 0) {
            wall_number = 1;
        }

        wall_part_result = wall_part_result + (wall_length * wall_height * wall_number);
    });
    if (wall_complete == 0) {
        return false;
    }

    var aperture_width = '';
    var aperture_height = '';
    var aperture_quantity = '';
    var aperture_result = 0;
    var aperture_complete = 1;
    $('.aperture_tr').each(function(i, elem) {

        aperture_width = $(this).find(".aperture_width").val().replace(/[,]+/g, '.');
        aperture_height = $(this).find(".aperture_height").val().replace(/[,]+/g, '.');
        aperture_quantity = $(this).find(".aperture_quantity").val().replace(/[,]+/g, '.');

        aperture_result = aperture_result + (aperture_width * aperture_height * aperture_quantity);
    });

    var mansard_width = '';
    var mansard_height = '';
    var mansard_height2 = '';
    var mansard_number = 0;
    var mansard_result = 0;
    var mansard_result2 = 0;
    var mansard_z = 0;
    var mansard_complete = 1;
    $('.mansard_tr').each(function(i, elem) {

        mansard_width = $(this).find(".mansard_width").val().replace(/[,]+/g, '.');
        mansard_height = $(this).find(".mansard_height").val().replace(/[,]+/g, '.');
        mansard_height2 = $(this).find(".mansard_height2").val().replace(/[,]+/g, '.');
        mansard_number = $(this).find(".mansard_number").val().replace(/[,]+/g, '.');
        if (mansard_number == 0) {
            mansard_number = 1;
        }

        mansard_result = mansard_result + ((mansard_height - mansard_height2) * (mansard_width * 0.5) * mansard_number);
        calc_result_numbers = mansard_result2 + (mansard_width * mansard_height2 * mansard_number);

    });

    var final_mansard = mansard_result + mansard_result2;

    var x = $("#wall_width").val();
    var s = '';
    var j = '';
    if (x == 25 || x == 30 || x == 50) {
        s = 26.666;
        j = 45;
    } else if (x == 15) {
        s = 53.333;
        j = 64;
    } else if (x == 35 || x == 40) {
        s = 28.57;
        j = 42;
    }

    var r2 = (wall_part_result - aperture_result + final_mansard) * x / 100;
    r2 = r2.toFixed(2);

    var r1 = Math.ceil(r2 * s);

    var r3 = Math.ceil(r1 / j);

    $("#result_1").val(r1);
    $("#result_2").val(r2);
    $("#result_3").val(r3);
    setTimeout(function() {
        $(".progress_containter").hide();
        $("#calc_result_numbers").show();
    }, 500)

}

$(document).ready(function() {

    $("#add_wall").click(function() {
        add_wall();
    });

    $("#add_aperture").click(function() {
        add_aperture();
    });

    $("#add_mansard").click(function() {
        add_mansard();
    });

    $(".calc_button").click(function() {
        calculate();
        return false;
    });

    $("#wall_width").change(function() {
        x = $(this).val();
        if (x == 25 || x == 30 || x == 50) {
            size_text = "500*250*300";
        } else if (x == 15) {
            size_text = "500*250*150";
        } else if (x == 35 || x == 40) {
            size_text = "400*350*250";
        }

        $(".size_text").html(size_text);

    });

});
