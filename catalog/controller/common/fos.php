<?php
class ControllerInformationFos extends Controller {

    private $error = array();
    
	public function index() {

        /*
        Хлебные крошки
        */
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
        );
        
        /*
            Подключаем к конролеру файл Language
        */
        $this->load->language('information/calc');

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_breadcrumbs'),
			'href' => $this->url->link('information/calc')
        );
        
        $this->document->setTitle('calc_setTitle');
        $this->document->setDescription('calc_meta_description');
        $this->document->setKeywords('calc_meta_keyword');

        /*
         Подключаем Модель
        */
        // $this->load->model('catalog/calc');


        $data['heading_title'] = $this->language->get('title');

        
        $this->response->setOutput($this->load->view('common/fos', $data));
    }
}