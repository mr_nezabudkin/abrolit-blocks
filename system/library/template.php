<?php
/**
 * @package		OpenCart
 * @author		Daniel Kerr
 * @copyright	Copyright (c) 2005 - 2017, OpenCart, Ltd. (https://www.opencart.com/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.opencart.com
*/

/**
* Template class
*/
class Template {
	private $adaptor;
	
	/**
	 * Constructor
	 *
	 * @param	string	$adaptor
	 *
 	*/
  	public function __construct($adaptor) {
	    $class = 'Template\\' . $adaptor;
		if (class_exists($class)) {
			$this->adaptor = new $class();
		} else {
			throw new \Exception('Error: Could not load template adaptor ' . $adaptor . '!');
		}
	}
	
	/**
	 * 
	 *
	 * @param	string	$key
	 * @param	mixed	$value
 	*/	
	public function set($key, $value) {

        /**
         * Добавляет файл стилей к шаблону в зависимости от URL
         */

	    if($key == 'styles'){

            $file_name = trim($_SERVER['REQUEST_URI'],'/');
            $file_name = str_replace('/', '__', $file_name);
            $full_pach = 'catalog/view/css/'.$file_name.'.css';

            if(file_exists(BASE_DIR.'/'.$full_pach)){
//
                $value[$full_pach] = [
                    'href' => $full_pach,
                    'rel' => 'stylesheet',
                    'media' => 'screen'
                ];
            }

        }
        if($key == 'scripts'){

            $file_name = trim($_SERVER['REQUEST_URI'],'/');
            $file_name = str_replace('/', '__', $file_name);
            $full_pach = 'catalog/view/javascript/'.$file_name.'.js';

            if(file_exists(BASE_DIR.'/'.$full_pach)){
//
                $value[$full_pach] = $full_pach;
            }

        }

        $this->adaptor->set($key, $value);
	}
	
	/**
	 * 
	 *
	 * @param	string	$template
	 * @param	bool	$cache
	 *
	 * @return	string
 	*/	
	public function render($template, $cache = false) {
		return $this->adaptor->render($template, $cache);
	}
}
